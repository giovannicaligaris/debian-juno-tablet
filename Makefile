SHELL := /bin/bash
# Install Juno-Tablet

DESTDIR=debian/juno-tablet

install-core:
	install -dm755 $(DESTDIR)/usr/bin
	install -dm755 $(DESTDIR)/usr/sbin
	install -dm755 $(DESTDIR)/usr/share/sounds/
	install -dm755 $(DESTDIR)/etc/xdg/autostart/
	install -dm755 $(DESTDIR)/etc/udev/rules.d/
	install -dm755 $(DESTDIR)/etc/udev/hwdb.d/
	install -dm755 $(DESTDIR)/etc/systemd/system
	install -dm755 $(DESTDIR)/usr/share/glib-2.0/schemas/
	install -dm755 $(DESTDIR)/etc/apt/sources.list.d/
	install -dm755 $(DESTDIR)/etc/environment.d/
	install -dm755 $(DESTDIR)/etc/pulse/default.pa.d/
	install -dm755 $(DESTDIR)/etc/apt/preferences.d/
	install -dm755 $(DESTDIR)/etc/systemd/system/powertop.service.d/
	install -dm755 $(DESTDIR)/etc/modprobe.d/
	install -dm755 $(DESTDIR)/usr/share/junocomp/
	install -dm755 $(DESTDIR)/etc/gnome-initial-setup
	install -dm755 $(DESTDIR)/etc/pipewire/pipewire-pulse.conf.d
	install -dm755 $(DESTDIR)/usr/lib/systemd/system-sleep
	install -dm755 $(DESTDIR)/etc/initramfs-tools/
	install -Dpm 0755 gaudible-deb.py $(DESTDIR)/usr/bin/gaudible-deb
	install -Dpm 0755 gaudible-flatpak.py $(DESTDIR)/usr/bin/gaudible-flatpak
	install -Dpm 0755 check-battery $(DESTDIR)/usr/bin/check-battery
	install -Dpm 0644 gaudible-deb.desktop $(DESTDIR)/etc/xdg/autostart/gaudible-deb.desktop
	install -Dpm 0644 gaudible-flatpak.desktop $(DESTDIR)/etc/xdg/autostart/gaudible-flatpak.desktop
	install -Dpm 0755 turbo/turbo-on $(DESTDIR)/usr/bin/turbo-on
	install -Dpm 0755 turbo/turbo-off $(DESTDIR)/usr/bin/turbo-off
	install -Dpm 0755 turbo/turbo-stat $(DESTDIR)/usr/bin/turbo-stat
	install -Dpm 0755 alsa $(DESTDIR)/usr/bin/alsa
	install -Dpm 0755 alsa-info $(DESTDIR)/usr/bin/alsa-info
	install -Dpm 0644 10_juno-debian-settings.gschema.override $(DESTDIR)/usr/share/glib-2.0/schemas/20_juno-debian-settings.gschema.override
	install -Dpm 0755 alsa $(DESTDIR)/usr/sbin/alsa
	install -Dpm 0644 rules/juno-pp.rules $(DESTDIR)/etc/udev/rules.d/juno-pp.rules
	install -Dpm 0644 rules/powertop.rules $(DESTDIR)/etc/udev/rules.d/powertop.rules
	install -Dpm 0755 powertop-usb-mouse $(DESTDIR)/usr/bin/powertop-usb-mouse
	install -Dpm 0644 rules/squeekboard.rules $(DESTDIR)/etc/udev/rules.d/squeekboard.rules
	install -Dpm 0644 pipewire/juno.pa $(DESTDIR)/etc/pulse/default.pa.d/juno.pa
	install -Dpm 0644 rules/external-display-power-profile.rules $(DESTDIR)/etc/udev/rules.d/external-display-power-profile.rules
	install -Dpm 0755 juno-pp $(DESTDIR)/usr/bin/juno-pp
	install -Dpm 0644 juno-pp.service $(DESTDIR)/etc/systemd/system/juno-pp.service
	install -Dpm 0755 juno-kd $(DESTDIR)/usr/bin/juno-kd
	install -Dpm 0644 juno-kd.service $(DESTDIR)/etc/systemd/system/juno-kd.service
	install -Dpm 0755 juno-monitor $(DESTDIR)/usr/bin/juno-monitor
	install -Dpm 0644 override.conf $(DESTDIR)/etc/systemd/system/powertop.service.d/override.conf
	install -Dpm 0644 sources/debian.sources $(DESTDIR)/etc/apt/sources.list.d/debian.sources
	install -Dpm 0644 sources/mobian.sources $(DESTDIR)/etc/apt/sources.list.d/mobian.sources
	install -Dpm 0755 terminal-clean $(DESTDIR)/usr/bin/terminal-clean
	install -Dpm 0644 61-sensor-local.hwdb $(DESTDIR)/etc/udev/hwdb.d/61-sensor-local.hwdb
	install -Dpm 0644 fan-blacklist.conf $(DESTDIR)/etc/modprobe.d/fan-blacklist.conf
	install -Dpm 0644 hid-blacklist.conf $(DESTDIR)/etc/modprobe.d/hid-blacklist.conf
	#install -Dpm 0755 juno-grub $(DESTDIR)/usr/share/junocomp/juno-grub
	install -Dpm 0644 initial-setup/vendor.conf $(DESTDIR)/etc/gnome-initial-setup/vendor.conf
	install -Dpm 0644 pipewire/pipewire-pulse.conf $(DESTDIR)/etc/pipewire/pipewire-pulse.conf.d/pipewire-pulse.conf
	install -Dpm 0755 suspend-usb-keyboard $(DESTDIR)/usr/lib/systemd/system-sleep/suspend-usb-keyboard
	install -Dpm 0755 disable-iwlwifi-suspend $(DESTDIR)/usr/lib/systemd/system-sleep/disable-iwlwifi-suspend

install: install-core

uninstall:
	rm -f $(DESTDIR)/usr/bin/check-battery
	rm -f $(DESTDIR)/etc/xdg/autostart/gaudible-flatpak.desktop
	rm -f $(DESTDIR)/etc/xdg/autostart/gaudible-deb.desktop
	rm -f $(DESTDIR)/usr/bin/gaudible-deb
	rm -f $(DESTDIR)/usr/bin/gaudible-flatpak
	rm -f $(DESTDIR)/usr/bin/turbo-on
	rm -f $(DESTDIR)/usr/bin/turbo-off
	rm -f $(DESTDIR)/usr/bin/turbo-stat
	rm -f $(DESTDIR)/etc/udev/rules.d/juno-turbo.rules
	rm -f $(DESTDIR)/usr/bin/alsa
	rm -f $(DESTDIR)/usr/share/glib-2.0/schemas/20_juno-debian-settings.gschema.override
	rm -f $(DESTDIR)/etc/apt/sources.list.d/debian-non-free.list
	rm -f $(DESTDIR)/etc/udev/hwdb.d/61-sensor-local.hwdb
	rm -f $(DESTDIR)/usr/sbin/alsa
	rm -f $(DESTDIR)/etc/udev/rules.d/powertop.rules
	rm -f $(DESTDIR)/etc/udev/rules.d/juno-pp.rules
	rm -f $(DESTDIR)/usr/bin/powertop-usb-mouse
	rm -f $(DESTDIR)/etc/systemd/system/powertop-usb-mouse.service
	rm -f $(DESTDIR)/etc/udev/rules.d/squeekboard.rules
	rm -f $(DESTDIR)/etc/pulse/default.pa.d/juno.pa
	rm -f $(DESTDIR)/etc/udev/rules.d/external-display-power-profile.rules
	rm -f $(DESTDIR)/usr/bin/alsa-info
	rm -f $(DESTDIR)/etc/apt/preferences.d/00-linux-juno
	rm -f $(DESTDIR)/usr/bin/juno-pp
	rm -f $(DESTDIR)/etc/systemd/system/juno-pp.service
	rm -f $(DESTDIR)/usr/bin/juno-kd
	rm -f $(DESTDIR)/etc/systemd/system/juno-kd.service
	rm -f $(DESTDIR)/usr/bin/juno-monitor
	rm -R $(DESTDIR)/etc/systemd/system/powertop.service.d/
	rm -f $(DESTDIR)/etc/apt/sources.list.d/debian.sources
	rm -f $(DESTDIR)/etc/apt/sources.list.d/mobian.sources
	rm -f $(DESTDIR)/usr/bin/terminal-clean
	rm -f $(DESTDIR)/usr/share/suspend-then-hibernate/juno-restore-alsa
	rm -f $(DESTDIR)/etc/modprobe.d/fan-blacklist.conf
	rm -f $(DESTDIR)/etc/modprobe.d/hid-blacklist.conf
	rm -R $(DESTDIR)/usr/share/junocomp/
	rm -R $(DESTDIR)/etc/gnome-initial-setup/
	rm -f $(DESTDIR)/etc/pipewire/pipewire-pulse.conf.d/pipewire-pulse.conf
	rm -f $(DESTDIR)/usr/lib/systemd/system-sleep/suspend-usb-keyboard
	rm -f $(DESTDIR)/usr/lib/systemd/system-sleep/disable-iwlwifi-suspend